const router = require('express').Router()
const post = require('./controllers/postController')
const user = require('./controllers/userController')

router.get('/', (req, res) =>
  res.status(200).json({
    status: 'success',
    message: "Hello World"
  })
)

// Post API collection
router.get('/posts', post.all)
router.post('/posts', post.new)

// User API collection
router.post('/user', user.register, user.sendEmail)

module.exports = router;
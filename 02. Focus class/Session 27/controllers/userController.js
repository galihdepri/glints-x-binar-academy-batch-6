const  User = require('../models/user')
const response = require('../helpers/response')
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

module.exports = {
    async register(req, res, next) {
        try {
            const user = await User.create({
                user_name: req.body.user_name,
                email: req.body.email,
                password: req.body.password                
            });
            
            
            let key = 'user';
            response(res, 201, key, user)
            }
    
        catch(err) {
            next(err)
        }
    },

    async sendEmail(req, res) {
        try {
            const msg = {
                to: req.body.email,
                from: 'test@example.com',
                subject: 'Hi! This is a test email!',
                text: 'Session 28 require me to send a confirmation email to user',
                html: '<strong>Session 28 require me to send a confirmation email to user</strong>',
              };

          await sgMail.send(msg);
        } catch (err) {
          console.error(err);
      
          if (err.response) {
            console.error(error.response.body)
          }
        }
    }
}

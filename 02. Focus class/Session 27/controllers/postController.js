const Post = require('../models/post')

module.exports = {
  all(req, res) {
    res.end()
  },

  new(req, res) {
    Post.create(req.body)
      .then(post => {
        res.status(201).json({
          status: 'success',
          data: { post }
        })
      })
      .catch(err => {
        res.status(422).json({
          status: 'fail',
          errors: [err.message]
        })
      })
  }
}
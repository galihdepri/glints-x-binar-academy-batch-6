module.exports = (res, code, key, data) => {
    res.status(code).json({
        status: 'success',
        [key]: data
    })
}
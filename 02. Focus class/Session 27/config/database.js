const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/session-27', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})
    .catch(err => {
        console.error(err.message)
        process.exit()
    })

mongoose.set('debug', true)
const mongoose = require('mongoose');
const Schema = mongoose.Schema

const postSchema = new Schema ({
    title: {
        type: 'string',
        required: true
    }, 
    body: {
        type: 'string',
        required: true
    }, 
    approved: {
        type: 'boolean',
        required: true,
        default: false
    }
})

const Post = mongoose.model('Post', postSchema);

module.exports = Post
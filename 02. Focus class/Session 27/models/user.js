const mongoose = require('mongoose');
const Schema = mongoose.Schema

const userSchema = new Schema ({
    user_name: {
        type: 'string',
        required: true
    }, 
    email: {
        type: 'string',
        required: true
    }, 
    password: {
        type: 'string',
        required: true
    }
})

const User = mongoose.model('User', userSchema);

module.exports = User
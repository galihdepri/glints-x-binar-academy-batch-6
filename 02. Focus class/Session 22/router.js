const router = require('express').Router()

// Controller
const index = require('./controllers/indexController.js')
const Post = require('./controllers/postController')

router.get('/', index.index)
router.get('/api/v1/posts', Post.index)
router.post('/api/v1/posts', Post.create)


module.exports = router; 
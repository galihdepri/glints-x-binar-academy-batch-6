const request = require('supertest')
const app = require('../server')
const {Post} = require('../models')


describe('Post API Collection', () => {

    beforeAll(() => {
        return Post.destroy({
            truncate: true
        })
    }),

    afterAll(() => {
        return Post.destroy({
            truncate: true
        })
    }),

    describe('Post /api/v1/posts', () => {
        test('Should successfully create a new post', done => {
            request(app).post('/api/v1/posts')
            .set('Content-Type', 'application/json')
            .send({ tittle: 'Hello World', body: 'this is hello world'})
            .then(res => {
                expect(res.body.status).toEqual('success');
                //expect(res.body.message).toEqual('Hello world')

                done()
            })
        })
    })
})
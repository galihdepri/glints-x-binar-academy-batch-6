const request = require('supertest');
const app = require('../server');

describe('Root endpoint testing', () => {

    describe('GET /', () => {

        test('Should return 200', done => {
            request(app).get('/')
            .then(res => {
                expect(res.body.status).toEqual('success');
                expect(res.body.message).toEqual('Hello world')

                done()
            })
        })
    })
})
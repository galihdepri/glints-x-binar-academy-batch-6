// HackerRank 2D Array DS

'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.replace(/\s*$/, '')
        .split('\n')
        .map(str => str.replace(/\s*$/, ''));

    main();
});

function readLine() {
    return inputString[currentLine++];
}

// Complete the hourglassSum function below.
function hourglassSum(arr) {
    let maxX = 3;
    let maxY = 3; 
    let total = -64; 
   

    for (let y = 0; y <= maxY; y++) {
        for (let x = 0; x <= maxX; x++) {
            
            // top hourglass
            let sum = arr[y][x] + arr[y][x+1] + arr[y][x+2];
            
            // middle hourglass
            sum += arr[y+1][x+1];
            
            // bottom hourglass    
            sum += arr[y+2][x] + arr[y+2][x+1] + arr[y+2][x+2]
            
        
            if (total < sum)
                 total = sum;
        }
    }
    
    return total;
}



function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

    let arr = Array(6);

    for (let i = 0; i < 6; i++) {
        arr[i] = readLine().split(' ').map(arrTemp => parseInt(arrTemp, 10));
    }

    let result = hourglassSum(arr);

    ws.write(result + "\n");

    ws.end();
}

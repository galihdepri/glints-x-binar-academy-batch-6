# Session 9 Summary

## Variables

Variables are something with assigned values. 
```js
// Variable Declaration
let name = "Galih";
// let name; = "Galih" // Assignment
```
Whereas **name** is assigned with a value of **Galih**.


### Naming of a variable 
```js
/ Mutable, camelCase
let isPasswordValid // is password valid: second and next word in capital case

// Immutable Variable of an alias, should always be in UPPERCASE_CONSTANT
const RED = "#ff0";

console.log("Hexdecimal of red color is", RED);
```
However, there's a restriction in naming the variable: **We cannot use number in the very first case**

```js
// Don't name it like this
// var 1Color = "This is error"; dont use number in first case;
var color1 = "This is working";
console.log(color1);
```

## Function
Function in javascript is a 'method' to run a defined operation. Function has two types:
* Function without argument
* Function with argument

In function without argument, we can simply run a function without any argument at all:
```js
// Declare a function
function hello() {
  console.log("Hello World!")
}

// Call that function
hello();
"Hello World!"
```
We can see that there's no argument there (). So when we run it, it would be a simple "Hello world".

Another one is function with argument:
```js
function f(x) {
  return x + 1;
}

console.log(f(2)); // 3
```

### Ways to create function
There are three ways to create function:
```js
function functionName() {
  // Do something
}
```
```js
const anotherFunction = function() {
  // Do something
}
```
```js 
// Lambda
const arrowFunction = () => {
  // Do Something
}
```

## If Else Statement
If else statement is a branching of which code we want to run
```js
const doesItRain = false;

if (doesItRain) {
  console.log("Use umbrella!");
}

let a = 2; // Will return Boolean DataTypes

if (a == 1) {
  // Do something
} else {
  // Do something
}
```

We also can use if else statement to validate the input from user. If the input should be a number, we can code it:
```js
let s = "Hello World";
let x = 4;

/*
 * Check data type of a variable or data
 * typeof data
 * */

// If s is a number, then run this code
if (typeof s == "number") {
  console.log(s + 10);
}

function example(x) {
  if (x >= 10) {
    return x / 2;
  } else {
    throw new Error("Input should have been more than 10");
  };
};

try {
  let result = example(9);
  console.log(result);
}

catch(err) {
  console.log(err.message);
}
```
// Variable Declaration
let name = "Galih";
// let name; = "Galih" // Assignment

var address = "Tangerang";
const isMarried = True;

// Mutable, camelCase
let isPasswordValid // is password valid: second and next word in capital case

// Immutable Variable of an alias, should always be in UPPERCASE_CONSTANT
const RED = "#ff0";

console.log("Hexdecimal of red color is", RED);

// Don't name it like this
// var 1Color = "This is error"; dont use number in first case;
var color1 = "This is working";
console.log(color1);

// $ _
const $TEST = "This is variable with $ sign";
console.log($TEST);

const _secret = "This is secret";
console.log(_secret);

const $ = "This is valid";
const _ = "This is also valid";
console.log($, _);

let a; // Without assigning value; let atau var
a = 10; // Assignment

console.log(a);

const c = 10; // Const is constant, 

let contoh = ""; // Data Types, Function, and Class

/*
 * Primitive DataTypes
 *
 * String   -> "Hello World" 
 * Number   -> 10 19 20 0.5
 * Boolean  -> true false
 *
 * Structured DataTypes
 * Array    -> [1,2,3,4], ["Hello World", 10, 100]
 * Object   -> { name: "Fikri", address: "Solo" }
 * Set      -> (1,2,3,4,5,6,7)
 * Symbol   -> 
 * */

let arr = [1,2,3,4,5]
console.log(arr);

let obj = {
  name: "Galih",
  address: "Tangerang"
}
console.log(obj);

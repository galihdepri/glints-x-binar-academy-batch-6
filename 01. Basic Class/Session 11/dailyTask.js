class Human {
    
    constructor (name, address, lang) {
        this.name = name
        this.address = address
        this.lang = lang
    }
    greet () {
        console.log(`Hi, my name is ${this.name}. Nice to meet you!`)
    }
    
    greetRosa() {
        console.log (`Hi ${Rosa.name}, my name is ${this.name}. How georgeous you are!`)
    }
    
    greetJuned() {
        console.log (`Hi ${this.name}! Thank you. You're nice`)
        }
    
    learnNewLang(newLang) {
        console.log(`${this.name} has decided to learn English`)
        console.log(`${Rosa.name} has decided to learn English`)

    }

    maryAnother() {
        console.log (`${this.name} is marrying ${Rosa.name} next month`)
    }
}

// Running the program
const Juned = new Human ("Juned", "Bandung", "Indonesian, English")
const Rosa = new Human ("Rosa", "Venice","Italian")

console.log(Juned)
console.log(Rosa)

// Each introduction
Juned.greet()
Rosa.greet()

// Greet each other

// Method 1: using prototype
Human.prototype.greetRo = function(name) {
    console.log(`Hi ${name}. I'm ${this.name}. You're so beautiful`); 
    }
    Juned.greetRo("Rosa") 

Human.prototype.greetJu = function(name) {
    console.log(`Hi ${name}! Thanks, that's so nice of you`)
    }
    Rosa.greetJu("Juned")

// Method 2: using instance

Juned.greetRosa()
Rosa.greetJuned()
console.log('\n');

console.log("To communicate clearly and effectively, both of them decided to learn English")
console.log('\n');

//Learning new language
Juned.learnNewLang('English')


// Mary another human
Juned.maryAnother()
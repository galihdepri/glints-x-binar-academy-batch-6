# Object Oriented Programing (Part 2): The 4 Pillars

In OOP, there are four pillars:
1. Inheritance
2. Encapsulation
3. Abstraction
4. Polymorphism

## Inheritance
The name tells itself: in this term, an object will inherit every behaviour and property of their parents' behaviour and property. But, we can add more properties and behaviours than the ones in the parent object.

```js
class Human {
  
  constructor(name, address) {
    this.name = name;
    this.address = address;
  }

  introduce() {
    console.log(`Hi, my name is ${this.name}`)
  }

  work() {
    console.log("Work!")
  }
}

// Create a child class from Human
class Programmer extends Human {

  constructor(name, address, programmingLanguages) {
    super(name, address) // Call the super/parent class constructor, in this case Person.constructor;
    this.programmingLanguages = programmingLanguages;
  }

  introduce() {
    super.introduce(); // Call the super class introduce instance method.
    console.log(`I can write a programming using these languages`, this.programmingLanguages);
  }

  code() {
    console.log(
      "Code some",
      this.programmingLanguages[
        Math.floor(Math.random() * this.programmingLanguages.length)
      ]
    )
  }
}

// Initiate from Human directly
let Obama = new Human("Barrack Obama", "Washington DC");
Obama.introduce() // Hi, my name is Barack Obama

let Fikri = new Programmer("Fikri Rahmat Nurhidayat", "Solo", ["Javascript", "Ruby", "Go", "Kotlin", "Python", "Elixir"]);
Fikri.introduce() // Hi, my name is Fikri; I can write a programming using these languages ["Javascript", "Ruby", "Go", "Kotlin", "Python", "Elixir"]
Fikri.code() // Code some Javascript/Ruby/...
Fikri.work() // Call super class method that isn't overrided or overloaded

try {
  // Obama can't code since Obama is an direct instance of Person, which don't have code method 
  Obama.code() // Error: Undefined method "code"
}

catch(err) {
  console.log(err.message)
}

console.log(Fikri instanceof Human) // true
console.log(Fikri instanceof Programmer) // true
```

## Encapsulation

Encapsulation is where you hide some method/property and it won't be accessible outside the class Declaration. It can be some procedure of a process and it can be a secret property. This is handy to prevent bugs, because you explicitly tell people to not use this method outside the class declaration. In ***short***, we make it private an inaccesible by other developer, unless it's meant to.

```js
class Human {

  static isLivingOnEarth = true;
  static group = "Vertebrate"; 
  
  constructor(name, isAlive) {
    this.name = name; // This will create a property called name
    this.isAlive = isAlive; //
  }

  // Instance method signature
  introduce() {
    console.log(`Hi, my name is ${this.name} and I'm ${this.isAlive ? "alive" : "dead"}`);
  }

  // Check if it can work
  checkWorkCapability() {
    if (!this.isAlive) {
      console.log("I'm dead, I obviously can't work!");
      return false;
    }
  }

  // Prepare for work
  prepareForWork() {
    console.log("Take a bath");
    console.log("Suit up!");
    console.log("On the way to the workplace");
  }

  doWork() {
    console.log("Coding");
    console.log("Get Stuck");
    console.log("Opened up Stack Overflow");
  }

  work() {
    if (!this.checkWorkCapability()) return; // Stop if he/she can't work
    this.prepareForWork();
    this.doWork();
  }

}

let mj = new Human("Michael Jackson", false);
mj.work() // Output: I'm dead, I obviously can't work!

// BUT, mj can access these following methods
mj.doWork();
mj.checkWorkCapability();
mj.prepareForWork();
```

## Abstraction
Abstraction is when you don't want to make changes to a class. If abstraction takes place in a class, we cannot instantiate objects under the class. 

```js
/*
  Abstract Class is a class where
  we can't instantiate an instance from it,
  unless it is instantiated from the sub class of that class.

  That means, everything that were defined inside the Abstract Class,
  were meant to be implemented by the sub class.

  So, abstract class is basically just a blueprint of another class
  */

// Abstract Class
class Human {
  
  static properties = [
    "name",
    "gender",
  ]

  constructor(props) {
    this._validate(props);

    /* Instance Properties */
    this.name = props.name;
    this.gender = props.gender;
  }

  _validate(props) {
    if (this.constructor === Human)
      throw new Error("Human is an abstract class!");

    if (typeof props !== 'object' || Array.isArray(props))
      throw new Error("Constructor needs object to work with");

    this.constructor.properties.forEach(i => {
      if (!Object.keys(props).includes(i))
        throw new Error(`${i} is required`);
    });
  }

  introduce() {
    console.log(`Hi, my name is ${this.name}`)
  }

  work() {
    console.log("Working...");
  }

}

/*
  We can't do this!
  new Human();
*/

/* ============================== */
class Police extends Human {
  static properties = [...super.properties, "rank"]

  constructor(props) {
    super(props);
    
    this.rank = props.rank;
  }

  shoot() {
    console.log("DOR!")
  }

  // Override method
  work() {
    this.shoot();
    super.work();
  }
}

class Densus88 extends Police {

  introduce() {
    super.introduce();
    console.log("And I'm a member of Densus88")
  }
}

const Wiranto = new Police({
  name: "Wiranto",
  gender: "Male",
  rank: "General"
})

Wiranto.work();

const Nurdin = new Densus88({
  name: "Nurdin",
  gender: "Male",
  rank: "General"
})

Nurdin.introduce();
Nurdin.shoot();

/* ============================== */

/* ============================== */

class Doctor extends Human {
  static properties = [...super.properties, "specialization"]

  constructor(props) {
    super(props);
    
    this.specialization = props.specialization;
  }

  cure(person) {
    console.log(`Cure ${person.name}`);
  }

  // Overload method
  work(person) {
    this.cure(person);
    console.log("Doing research...")

    super.work();
  }
}

const Alodoc = new Doctor({
  name: "Alodoc",
  gender: "Male",
  specialization: "Dentist"
})

Alodoc.work(Wiranto);
```
## Polymorphism

Polymorphism means children classes usually act very different from its parent, or even, not the same at all. There are tons of implementation of this concept that you can use, but in this case we use the *mix-ins* implementation.
Let's say you have 4 classes, Human (Abstract), Doctor, Police, Writer, and Army. Doctor, Police and Army are the children of Human. Army and Police can shoot, but the Doctor doesn't. But, Army, Police, and Doctor can physically save people. To do this kind of code, we need mix-ins.

```js
class Human {
  constructor(props) {
    // Do something
  }

  introduce() {
    // Do something
  }

  work() {
    console.log(`${this.constructor.name}:`,"Working....")
  }
}

//------------------------------------------------
// Public Server Module/Helper
const PublicServer = Base => class extends Base {
  save() {
    console.log("SFX: Thank You!")
  }
}

// Military Module/Helper
const Military = Base => class extends Base {
  shoot() {
    console.log("DOR!")
  }
}
//------------------------------------------------

class Doctor extends PublicServer(Human) {
  constructor(props) {
    super(props);
  }

  work() {
    super.work(); // From Human Class
    super.save(); // From Public Server Class
  }
}

class Police extends PublicServer(Military(Human)) {
  static workplace = "Police Station";

  constructor(props) {
    super(props);
    this.rank = props.rank;
  }

  work() {
    super.work();
    super.shoot(); // From Military class
    super.save(); // From Public Server Class 
  }
}

class Army extends PublicServer(Military(Human)) {
  static workplace = "Police Station";

  constructor(props) {
    super(props);
    this.rank = props.rank;
  }

  work() {
    super.work();
    super.shoot(); // From Military class
    super.save(); // From Public Server Class 
  }
}

class Writer extends Human {
  
  work() {
    console.log("Write books");
    super.work();
  }
}

/* Instantiate Military Based Class */
const Wiranto = new Police({
  name: "Wiranto",
  address: "Unknown",
  rank: "General"
})

const Prabowo = new Army({
  name: "Prabowo",
  address: "Unknown",
  rank: "General"
})
/* --------------------------------- */

/* -----Instantiate Doctor------ */
const Bambang = new Doctor({
  name: "Andika Bambang",
  address: "Kartopuran"
})
/* ----------------------------- */

/* -----Instantiate Writer------ */
const Noah = new Writer({
  name: "Yuval Noah Harari",
  address: "Sangiran"
})
/* ----------------------------- */

Wiranto.shoot(); // Work
Prabowo.shoot(); // Work

Wiranto.save() // Work
Prabowo.save() // Work
Bambang.save() // Work

Wiranto.work() // Work
Prabowo.work() // Work
Bambang.work() // Work
Noah.work() // Work 
```
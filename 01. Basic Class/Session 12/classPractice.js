class Human  {
    
    static properties = [
    "name",
    "address"
    
    ]

    constructor(props) {
        // Validate the props
        this._validate(props);
    
        this.name = props.name;
        this.address = props.address;

      }
    
      // Validate, for input inside the constructor
      _validate(props) {
        if (typeof props !== 'object' || Array.isArray(props))
          throw new Error("cuisines must be an array");
    
        this.constructor.properties.forEach(i => {
          if (!Object.keys(props).includes(i))
            throw new Error(`${i} is required`);
        });
      }

    introduce() {
        console.log(`Hi my name is ${this.name}`)
    }

    cook() {
        console.log(`${this.name} can cook food`)
    }
}

class Chef extends Human {

    static properties = [...super.properties, "cuisines"]

    constructor(props) {
        super(props); // What is super?
        this.cuisines = props.cuisines;
        this.types = props.types
      }

 
    cook() {
        super.cook()
        console.log("and it's better")
    }

    promote() {
        console.log(`${this.name} can cook ${this.cuisines} and he's specialized in ${this.types} food`)

    }

    introduce() {
        super.introduce()
        console.log(`and I'm specialized in ${this.types} food`)
        
    }
}

let Galih = new Chef ({
    name: "Galih",
    address: "Tangerang",
    cuisines: ["desserts, main dishes"],
    types: ["Italian"]
}) 

Galih.cook()
Galih.promote()
Galih.introduce()
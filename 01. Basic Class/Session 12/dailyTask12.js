const fs = require('fs');

class Record {
  constructor(props) {
    if (this.constructor == Record) 
      throw new Error("Can't instantiate from Record");
    
    this._validate(props);
    this._set(props);
  }

  _validate(props) {
    if (typeof props !== 'object' || Array.isArray(props))
      throw new Error("Props must be an object");
    

    this.constructor.properties.forEach(i => {
      if (!Object.keys(props).includes(i))
        throw new Error(`${this.constructor.name}: ${i} is required`)
    })
  }

  _set(props) {
    this.constructor.properties.forEach(i => {
      this[i] = props[i];
    })
  }

  get all() {
    try {
      return eval(
        fs.readFileSync(`${__dirname}/${this.constructor.name}.json`)
          .toString()
      )
    }
    catch {
      return []
    }
  }

  find(id) {

  }

  update(id) {

  }

  delete(id) {

  }

  save() {
    fs.writeFileSync(
      `${__dirname}/${this.constructor.name}.json`,
      JSON.stringify([...this.all, { id: this.all.length + 1, ...this } ], null, 2)
    );
  }
}

class User extends Record {

  static properties = [
    "email",
    "password"
  ]
}

let Fikri = new User({
  email: "test01@mail.com",
  password: "123456"
});

let Fikri2 = new User({
  email: "test02@mail.com",
  password: "123456"
});

Fikri.save();
Fikri2.save();

/*
  Make two class who inherit Abstract Class called Record 

  Book,
    title
    author
    price
    publisher

  Product,
    name,
    price,
    stock
*/

// Sub class Book

class Book extends Record {
    static properties = [
        "title", 
        "author",
        "price",
        "publisher"
    ]
   
    save() {
        super.save();
    }


}

let bookOne = new Book ({
    title       : "Syntactic Structure",
    author      : "Noam Chomsky",
    price       : "$11.95",
    publisher   : "Martino Fine Books"
    
}) 

let bookTwo = new Book ({
    title       : "My First Coding Book",
    author      : "Kiki Prottsman",
    price       : "$29.87",
    publisher   : "DK CHILDREN"
})

console.log(bookOne)
console.log(bookTwo)

bookOne.save()
bookTwo.save()



// Sub Class Product 
class Product extends Record {
    static properties = [
        "name",
        "price",
        "stock"
    ]
    
    save() {
        super.save();
    }
}

let product1 = new Product ({
    name    : "Rexona Men",
    price   : "IDR 20.000",
    stock   : "50"
})

let product2 = new Product ({
    name    : "Kapur Bagus",
    price   : "IDR 15.000",
    stock   : "20"
})

console.log(product1)
console.log(product2)

product1.save()
product2.save()
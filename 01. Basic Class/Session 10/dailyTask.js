/*
 * DON'T CHANGE
 * */

function randomNumber() {
    let random = Math.floor(Math.random() * 100);
  
    // Recursive
    if (!random) {
      return randomNumber();
    }
  
    return random
  }
  
  function sample(arr) {
    return arr[
      Math.floor(
        Math.random() * arr.length
      )
    ]
  }
  
  function createArrayElement() {
    let random = randomNumber();
    let props = [null, random]
    let name = ["Ahmad", "Michael Jackson", "Armand", "Maulana", "Ryan", "Gosling", null]
  
    return {
      name: sample(name),
      luckyNumber: sample(props)
    }
  }
  
  function createArray() {
    let data = []
  
    for (let i = 0; i < randomNumber(); i++) {
      data.push(createArrayElement())
    }
  
    return data;
  }
  
  const arr = createArray()
  
  /*
   * Code Here!
   * */
  console.log('Raw:', arr);
  
  function clean(data) {
    let result = []

    for (let i=0; i<data.length; i++) { //I went through objects that are null and ignore the null value using if statement
        if (data[i].name !== null && data[i].luckyNumber !== null)
        result.push(data[i])
    }
    // Code here
  
    return result;
  }
  
  /*
   * DON'T CHANGE
   * */
  
  if (process.argv.slice(2)[0] == "test") {
    try {
      let result = clean(arr);
      console.log('Result:', result);
  
      if (!result.length) {
        throw new Error("Array has no data")
      }
  
      result.forEach(i => {
        if (!i.name || !i.luckyNumber) {
          throw new Error("Array still contains null")
        }
      })
  
      console.log('Done:', true);
    }
  
    catch(err) {
      console.error(err.message);
      console.log('Done:', false);
    }
  }
  
# Session 10 Summary: 

### Loop
In this session I learned basics of looping: ***do***, ***for***, and ***while***. Loop is used to repeat specific statement unti it meets specific condition the developer set (break condition)2.

**Do loop**

```js
/*
 * do loop
 * */

let i = 0;
do {
  console.log("Hello World");
  i++
} while (i < 10);
```

**For loop**
```js
/*
 * for loop
 * */

for (i = 0; i < 10; i += 2) {
  console.log(i) // Always increment
  console.log("Ini putaran ke", i + 1);
}
```

**While loop**
```js
/*
 * while loop
 * */

let i = 0; // Global Variable
while(i < 10) {
  console.log("Before:", i)

  i++; // Statement to make the loop going to break

  console.log("After:", i)
}
```

For loop and while loop are usually used for looping an array. The difference between those two lies in the break statement.

For looping array:
```js
let arr = [8,9,10,5,2,0,6,10];

for (let i = 0; i < arr.length; i++) {
  let elemen = arr[i];
  let result = elemen * 2;
  console.log(result);
}
```

While looping array:
```js
let arr = [8,9,10,5,2,0,6,10];

i = 0;
while (i < arr.length) {
  console.log(arr[i] * 2);
  i++;
}
```
### Array mapping
Array mapping is a loop used to mutate the array element. Meaning, if we want to change (or update) the elements of an array, we could use **array mapping**. 

```js
for (let i = 0; i < arr.length; i++) {
  arr[i] = arr[i] * 2; // Directly change the array
}
console.log('Mutated Array:', arr);

let result = []; // We will push elements in there
for (let i = 0; i < arr.length; i++) {
  result.push(arr[i] * 2);
}
console.log('New Array:', result);
console.log('Mutated Array:', arr);
```

### Filtering an array
To select or filter specific elements in array and update it, we could use below function. 

```js
arr = [1,2,3,4,5,6,7,8,9,10];
result = [];
for (let i = 0; i < arr.length; i++) {
  if (i >= 6)
    result.push(arr[i]);
}
```
For an array whose elements are objects, we can use the following:

```js
console.log('Raw:', arr);
console.log('Result:', result);

arr = [
  {
    name: "Fikri",
    rank: 10,
  },
  {
    name: "Aron",
    rank: 9
  },
  {
    name: "Armand",
    rank: 8
  },
  {
    name: "Rasyid",
    rank: 10
  },
  {
    name: "Fadli",
    rank: 11
  },
  {
    name: "Galih",
    rank: 5
  },
  {
    name: "Michael",
    rank: 89
  },
]


result = [];
for (let i = 0; i < arr.length; i++) {
  if (arr[i].rank >= 10)
    result.push(arr[i]);
}

console.log('Raw:', arr); // Showing unchanged array
console.log('Result:', result); //Showing filtered array
```

### Array methods in javascript
Javascript has a built-in method for array filtering, mapping, looping, and sorting.

* Filtering
```js
let result = arr.filter(i => i > 500);
console.log('Filtered:', result);
```

* Mapping
```js
result = arr.map(i => i * 2);
console.log('Mapped:', result);
```

* For each method
```js
arr.forEach(i => console.log(i))
```

* Sorting
```js
result = arr.sort((a, b) => a - b);
console.log('ASC:', result);

result = arr.sort((a, b) => b - a);
console.log('DSC:', result);
```

## Swapping a value
We can swap a value with another one in javascript by using following formula:
```js
let c = a;

a = b;
b = c;
```
It is mandatory to create *c* as a backup value. It means that the value has to be stored somewhere before it could be used into a new variable. If there's no *c*, swapping won't take place.

```js
// Swap a value with b value;
let a = 10;
let b = 20;
console.log('a Before:', a); 
console.log('b Before:', b);
```
We can also swap array in javascript:
```js
// Swap array?
let arr = ["Hello World", "Goodbye World", "Kata-kata Lain"];
let tmp = arr[1]; // Backup
arr[1] = arr[0];
arr[0] = tmp;
console.log('Swapped:', arr);
```

## Exporting and importing modules
It won't be efficient if we creat all codes in just one single file. Multiple files are to be created to make coding more eficient and effective. So what if we want to use some function, objects, etc in other file that we need? We can use export/import module.

To export specific module in a file, we can use:
```js
module.exports = arr //variable
```
and to import it, we use
```js
const array = require('./array.js'); //state the name of the wanted file along with its extension
```

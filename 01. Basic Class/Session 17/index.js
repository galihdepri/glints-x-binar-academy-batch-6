const express = require('express');
const app = express()
const router = require('./router.js');

const dotenv = require('dotenv');
dotenv.config();

const morgan = require('morgan')

// Middleware to Parse JSON
app.use(morgan('tiny'))
app.use(express.json());
 
// GET /
app.get('/', function (req, res) {
  res.json({
    status: true,
    message: 'Hello World'
  })
})

app.use('/', router);

app.get('/errors', (req, res) => {
  throw new Error("It's error")
})

app.use((err, req, res, next) => {
  res.status(500).json({
    status: 'fail',
    errors: [err.message]
  })
})
 
app.listen(3000, () => {
  console.log("Listening on port 3000!");
})
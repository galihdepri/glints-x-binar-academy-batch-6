const { User } = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = {

  create(req, res) {
    /*
      We can't save user's passwordas plain text
      We should encrypt that password
      before saving it to the DB.
      For safety reason.
    */

    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(req.body.password, salt); // TO ecnrypt the password
   
    User.create({
      email: req.body.email,
      encrypted_password: hash
    })
      .then(user => {
        res.status(201).json({
          status: 'success',
          data: {
            user
          }
        })
      })
      .catch(err => {
        res.status(422).json({
          status: 'fail',
          errors: [err.message]
        })
      })
  },

  register(req, res) {
    /*
      We can't save user's password as plain text
      We should encrypt that password
      before saving it to the DB.
      For safety reason.
    */

    req.body.email = req.body.email || "";

    User.create({
      email: req.body.email.toLowerCase(),
      encrypted_password: req.body.password
    })
      .then(user => {
        res.status(201).json({
          status: 'success',
          data: {
            user: user.entity
          }
        })
      })
      .catch(err => {
        res.status(422).json({
          status: 'fail',
          errors: [err.message]
        })
      })
  },

  login(req, res) {
    /*

      1. Find the user by email [X]
      2. Get the user's encrypted_password property [X]
      3. Compare req.body.password with the encrypted_password 


      // ===================
      4. Create access token 
      5. Serve the response
      // ===================

    */

    User.authenticate(req.body)
      .then(data => {
        res.status(201).json({
          status: 'success',
          data: data.entity
        })
      })
      .catch(err => {
        res.status(401).json({
          status: 'fail',
          errors: [err.message]
        })
      })
  },

  me(req, res) {
    let token = req.headers.authorization;

    if (!token) return res.status(401).json({
      status: 'fail',
      errors: ["Invalid Token"]
    })
    
    try {
      let payload = jwt.verify(token, process.env.SECRET_KEY);
    }

    catch(err) {
      return res.status(401).json({
        status: 'fail',
        errors: ["Invalid Token"]
      })
    }
    
    User.findByPk(payload.id)
      .then(user => {
        res.status(201).json({
          status: 'success',
          data: {
            user
          }
        })
      })
  }

}
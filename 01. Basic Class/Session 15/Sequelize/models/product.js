'use strict';
module.exports = (sequelize, DataTypes) => {
  const product = sequelize.define('product', {
    price: DataTypes.INTEGER,
    stock: DataTypes.INTEGER,
    verified: DataTypes.BOOLEAN
  }, {
    underscored: true,
  });
  product.associate = function(models) {
    // associations can be defined here
  };
  return product;
};
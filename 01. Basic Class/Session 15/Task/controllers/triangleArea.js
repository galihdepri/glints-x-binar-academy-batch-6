const triangle = require('../db/triangle.json');
module.exports = {
    triangleArea(req, res){
        let result = 0.5 * req.body.H * req.body.B;
        return res.status(200).json({
            "Name": 'Triangle',
            "Height": req.body.H,
            "Base": req.body.B,
            "Area": result
        })
    }
}

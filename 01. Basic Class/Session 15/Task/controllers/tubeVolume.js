const tube = require('../db/tube.json');
module.exports = {
    tubeVolume(req, res){
        let result = req.body.Phi * req.body.R * req.body.R * req.body.H;
        return res.status(200).json({
            "Name": 'Tube',
            "Phi": req.body.Phi,
            "R": req.body.R,
            "Height": req.body.H,
            "Volume": result
        })
    }
}

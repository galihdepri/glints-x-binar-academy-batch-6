const circle = require('../db/circle.json');
module.exports = {
    circleArea(req, res){
        let result = req.body.Phi * (req.body.R * req.body.R)
        return res.status(200).json({
            "Name": 'Circle',
            "Phi": req.body.Phi,
            "R": req.body.R,
            "Area": result
        })
    }
}

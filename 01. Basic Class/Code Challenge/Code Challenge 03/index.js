const Product = require('./products.json')
const User = require('./user.json')
const http = require('http');
const app = http.createServer(handler)
/*
  Code Challenge #3

  Your goals to create these endpoint
    
    /products
      This will show all products on products.json as JSON Response

    /products/available
      This will show the products which its stock more than 0 as JSON Response

    /users
      This will show the users data inside the users.json,
      But don't show the password!

  */



/* Code Here */ 

function handler(req, res) {
  switch(req.url) {
    case '/':
      res.write("Hello World");
      break;

    case '/products':
      res.write(JSON.stringify(Products.all));
      break;

    case '/products/available':
      res.write(JSON.stringify(Products.check(0)));
      break;

      case '/users':
        res.write(JSON.stringify(User.all,['id','name','email','role']));

      break;

  }

  res.end();
}


app.listen(5000, () => {
  console.log("Listening on port 5000");
})
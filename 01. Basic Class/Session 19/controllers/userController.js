const { User } = require('../models');

exports.register = async function(req, res, next) {
  try {
    const user = await User.create({
      email: req.body.email,
      encrypted_password: req.body.password
    })

    res.data = user;
    next();
  }

  catch(err) {
    res.status(422);
    next(err);
  }
}

exports.login = async function(req, res, next) {
  try {
    const user = await User.authenticate(req.body);
    res.status(201);
    res.data = { user: user.entity }
    next()
  }

  catch(err) {
    res.status(401);
    next(err);
  }
}
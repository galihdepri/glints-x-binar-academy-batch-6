const jwt = require('jsonwebtoken');
const { User } = require('../models')
const dotenv = require('dotenv');
dotenv.config();

module.exports = async(req,res,next) => {
    try {
        let token = req.headers.authorization;
        let payload = jwt.verify(token, process.env.SECRET_KEY);
        req.user = await User.findByPk(payload.id)
        next();
    }
    
    catch {
        res.status(401);
        next(new Error("Invalid token"))
    }
}
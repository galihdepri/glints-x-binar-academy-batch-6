const { Post } = require('../models')
const jwt = require('jsonwebtoken');
const { User } = require('../models')
const dotenv = require('dotenv');
dotenv.config();

module.exports = async(req,res,next) => {
    try {
        let posting = await Post.findByPk(req.params.id);
        if(posting.user_id == req.user.id){
            next()
        }else {
            res.status(403);
            next(new Error("Oops! This post isn't yours!"));
          }
    } 
    
    catch (error) {
              res.status(401);
              next(error);
        }
    
}

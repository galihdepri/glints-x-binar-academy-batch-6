// Third Party Module
const express = require('express');
const morgan = require('morgan');
const dotenv = require('dotenv');
dotenv.config();

// Core Module
const exception = require('./middlewares/exception');
const router = require('./router');

const app = express();
const { PORT = 3000 } = process.env;

app.get('/', (req, res) => {
  res.status(200).json({
    status: 'success',
    message: "Hello World"
  })
})

app.use(express.json());
app.use(morgan('dev'));
app.use('/api/v1', router);

// Apply Exception Handler
exception.forEach(handler =>
  app.use(handler)
);

app.listen(PORT, () => {
  console.log(`Server started at ${Date()}`);
  console.log(`Listening on port ${PORT}`);
})
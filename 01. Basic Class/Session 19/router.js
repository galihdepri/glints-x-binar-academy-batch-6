const router = require('express').Router();

// Core Module
const userController = require('./controllers/userController');
const postController = require('./controllers/postController')

// Middlewares
const success = require('./middlewares/success');
const authenticate = require('./middlewares/authentication')
const authorization = require('./middlewares/authorization')

// User Collection API
router.post('/users/register', userController.register, success);
router.post('/users/login', userController.login, success)

// Posts Collection API
router.post('/posts', authenticate, postController.create, success)
router.get('/posts', postController.findAll, success)
router.put('/posts/:id', authenticate, authorization, postController.update, success)
router.delete('/posts/:id', authenticate, authorization, postController.delete, success)

module.exports = router;
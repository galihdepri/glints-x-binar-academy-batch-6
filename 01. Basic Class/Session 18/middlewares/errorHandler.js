module.exports = [
  function(req, res, next) {
    res.status(404);
    next(new Error("Not Found!"))
  },

  function(err, req, res, next) {
    if (res.statusCode == 200) res.status(500);

    res.status(res.statusCode).json({
      status: 'fail',
      errors: [err.message]
    })
  }
]
const express = require('express');
const router = express.Router();
const product = require('./controllers/productController');
const user = require('./controllers/userController');
const authenticate = require('./middlewares/authenticate');
const presentHandler = require('./middlewares/presentHandler');

/* Product API Collection */
router.get('/products',product.all, presentHandler);
router.get('/products/available', product.available, presentHandler);
router.get('/product', product.show, presentHandler); // It could be merged in product.all handler
router.post('/products',authenticate ,product.create,presentHandler);
router.put('/products/:id',authenticate, product.update, presentHandler);
router.delete('/products/:id',authenticate, product.delete, presentHandler);

/* User API Collection */
router.post('/users/register', user.register, presentHandler);
router.post('/users/login', user.login, presentHandler);
router.get('/users/me',authenticate ,user.me, presentHandler);

module.exports = router;

const { User } = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = {

  async register(req, res) {
    /*
      We can't save user's password as plain text
      We should encrypt that password
      before saving it to the DB.
      For safety reason.
    */

    req.body.email = req.body.email || "";

    try {
      let user = await User.create({
        email: req.body.email.toLowerCase(),
        encrypted_password: req.body.password
      })

      res.status(201).json({
        status: 'success',
        data: {
          user
        }
      })
    }

    catch(err) {
      next(err);
    }
  },

  login(req, res) {
    /*

      1. Find the user by email [X]
      2. Get the user's encrypted_password property [X]
      3. Compare req.body.password with the encrypted_password 


      // ===================
      4. Create access token 
      5. Serve the response
      // ===================

    */

    User.authenticate(req.body)
      .then(data => {
        res.status(201).json({
          status: 'success',
          data: data.entity
        })
      })
      .catch(err => {
        res.status(401);
        next(err);
      })
  },

  me(req, res) {    
    res.status(200).json({
      status: 'success',
      data: {
        user: req.user
      }
    })
  }

}
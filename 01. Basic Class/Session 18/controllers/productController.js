const { Product } = require('../models');
const { Op } = require('sequelize');
const jwt = require('jsonwebtoken');

module.exports = {

  all(req, res) {
    Product.findAll().then(data => {
      res.status(200);
      req.data = data;
      next()
    })
  },

  // Example of using Query Params
  show(req, res) {
    Product.findOne({
      where: {
        id: req.query.id
      }
    })
      .then(product => {
        res.status(200).json({
          status: 'success',
          data: {
            product
          }
        })
      })
  },

  create(req, res) {
    Product.create(req.body)
      .then(data => {
        res.status(201).json({
          status: "success",
          data: {
            product: data
          }
        });
      })
      .catch(err => {
        res.status(422).json({
          status: 'fail',
          errors: [err.message]
        });
      })
  },

  // Example of using Path Params
  update(req, res) {
    Product.update(req.body, {
      where: {
        id: req.params.id // :id
      }
    })
      .then(data => {
        res.status(200).json({
          status: "success",
          message: `Product with ID ${req.params.id} is succesfully updated!`
        })
      })
      .catch(err => {
        res.status(422).json({
          status: 'fail',
          errors: [err.message]
        })
      })
  },

  available(req, res) {
    Product.findAll({
      where: {
        stock: {
          [Op.gt]: 0
        }
      }
    })
      .then(products => {
        res.status(200).json({
          status: 'success',
          data: {
            products
          }
        })
      })
  },

  delete(req, res) {
    console.log(req.random);
    Product.destroy({
      where: {
        id: req.params.id
      }
    })
      .then(() => {
        res.status(204).json({
          status: 'success',
          message: `Product with ID ${req.params.id} is succesfully deleted!`
        })
      })
      .catch(err => {
        res.status(422).json({
          status: 'fail',
          errors: [err.message]
        })
      })
  }
}
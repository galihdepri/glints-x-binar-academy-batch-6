const express = require('express');
const app = express()
const router = require('./router.js');

// Middleware to Parse JSON
app.use(express.json());
 
// GET /
app.get('/', function (req, res) {
  res.json({
    status: true,
    message: 'Hello World'
  })
})

app.use('/', router);
 
app.listen(3000, () => {
  console.log("Listening on port 3000!");
})
const { User } = require('../models');
const bcrypt = require('bcryptjs');

module.exports = {

  create(req, res) {
    /*
      We can't save user's passwordas plain text
      We should encrypt that password
      before saving it to the DB.
      For safety reason.
    */

    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(req.body.password, salt); // TO ecnrypt the password
   
    User.create({
      email: req.body.email.toLowerCase,
      encrypted_password: hash
    })
      .then(user => {
        res.status(201).json({
          status: 'success',
          data: {
            user
          }
        })
      })
      .catch(err => {
        res.status(422).json({
          status: 'fail',
          errors: [err.message]
        })
      })
  },

  register(req, res) {
    /*
      We can't save user's passwordas plain text
      We should encrypt that password
      before saving it to the DB.
      For safety reason.
    */
    User.register(req.body)
      .then(user => {
        res.status(201).json({
          status: 'success',
          data: {
            user: user.entity
          }
        })
      })
      .catch(err => {
        res.status(422).json({
          status: 'fail',
          errors: [err.message]
        })
      })
  },

  login(req, res) {
    /*

      1. Find the user by email [X]
      2. Get the user's encrypted_password property [X]
      3. Compare req.body.password with the encrypted_password 
      4. Serve the response

      // ===================
      5. Create access token 
      // ===================

    */

    User.findOne({
      where: {
        email: req.body.email
      }
    })
      .then(user => {
        if (!user)
          return res.status(401).json({
            status: 'fail',
            errors: ["Email doesn't exist!"]
          })

        const isPasswordCorrect = bcrypt.compareSync(req.body.password, user.encrypted_password);

        if (!isPasswordCorrect)
          return res.status(401).json({
            status: 'fail',
            errors: ["Wrong password!"]
          })

        res.status(201).json({
          status: 'success',
          message: "Succesfully logged in!",
          data: {
            user: user.entity
          }
        })
      })
      .catch(err =>
        res.status(400).json({
          status: 'fail',
          errors: [err.message]
        })
      )

  }

}
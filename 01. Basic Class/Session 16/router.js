const express = require('express');
const router = express.Router();
const product = require('./controllers/productController');
const user = require('./controllers/userController');

/* Product API Collection */
router.get('/products', product.all);
router.get('/products/available', product.available);
router.get('/product', product.show); // It could be merged in product.all handler
router.post('/products', product.create);
router.put('/products/:id', product.update);
router.delete('/products/:id', product.delete);

/* User API Collection */
router.post('/users/register', user.register);
router.post('/users/login', user.login);

module.exports = router;
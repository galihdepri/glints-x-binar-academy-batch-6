const cube = require('../db/cube.json');
module.exports = {
    cubeVolume(req, res){
        let result = Math.pow(req.body.E,3);
        return res.status(200).json({
            "Name": 'Cube',
            "Side": req.body.E,
            "Volume": result
        })
    }
}

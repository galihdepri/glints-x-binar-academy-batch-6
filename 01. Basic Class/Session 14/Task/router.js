const express = require('express');
const router = express.Router();

const {
    info
} = require('./info.js')
const {
    circleArea
} = require('./controllers/circleArea.js');
const {
    squareArea
} = require('./controllers/squareArea.js');
const {
    cubeVolume
} = require('./controllers/cubeVolume.js');
const {
    triangleArea
} = require('./controllers/triangleArea.js');
const {
    tubeVolume
} = require('./controllers/tubeVolume.js');


router.get('/', info);
router.post('/calculate/circleArea', circleArea);
router.post('/calculate/squareArea', squareArea);
router.post('/calculate/cubeVolume', cubeVolume);
router.post('/calculate/triangleArea', triangleArea);
router.post('/calculate/tubeVolume', tubeVolume);

module.exports = router;

'use strict';
module.exports = (sequelize, DataTypes) => {
  const Post = sequelize.define('Post', {
    title: DataTypes.STRING,
    body: DataTypes.TEXT,
    user_id: DataTypes.INTEGER
  }, {

  });


  Post.associate = function(models) {
    // one to many example
    /* user has many posts, each post belongs to User 
    */
    
    Post.belongsTo(models.User, {
      foreignKey: 'user_id',
      as: 'author'
    })

  };
  return Post;
};
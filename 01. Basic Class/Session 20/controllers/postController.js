const { Post, User } = require ('../models')

exports.create = async function(req, res, next) {
    try {
        const post = await Post.create({
            title: req.body.title,
            body: req.body.body,
            user_id: req.user.id
        });
        res.status(200);
        res.data = { post };
        next()
    }

    catch(err) {
        res.status(422);
        next(err)
    }
}

exports.findAll = async function(req, res, next) {
    try {
        const posts = await Post.findAll({
            include: 'author'
        })
        res.status(200);
        res.data = { posts };
        next()
    }

    catch(err) {
        res.status(404);
        next(err)
    }
}

exports.update = async function(req, res, next) {
    /*
    Create authorization so 
    other users can't update other user's posts
    */
    try {
        await Post.update(req.body, { 
            where: { id: req.params.id}
    });
        res.status(200);
        res.data = "Succesfully updated";
        next()
    }

    catch(err){
        res.status(422);
        next(err);
    }
}

exports.delete = async function(req, res, next) {
    //finish this code
    try {
        await Post.destroy({
            where: {
                id: req.params.id
            }
        })
        res.status(204);
        res.end
    }

    catch(error) {
        res.status(400);
        next(error);    
    }
}